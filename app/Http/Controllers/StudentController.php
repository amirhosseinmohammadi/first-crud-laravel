<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{

    public function home()
    {
        return view('students.home');
    }

    public function index()
    {
        $students = Student::all();
        return view('students.show' , compact('students'));
    }
    public function show()
    {
        $students = DB::table('students')->orderBy('point' , 'desc')->take(4)->get();
        $low = DB::table('students')->orderBy('point' , 'asc')->take(3)->get();
        $avg = DB::table('students')->avg('point');
        return view('students.all' , compact('students' , 'low' , 'avg'));
    }

    public function all()
    {
        return view('students.all');
    }

    public function create(){
        return view('students.create');
    }

    public function store(Request $request){
        Student::create($request->all());
        return redirect()->route('students.index')->with('success' , 'عملیات ایجاد با موفقیت انجام شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index')->with('success' , 'عملیات حذف با موفقیت انجام شد');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.edit' , compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $student ->update([
           'first_name' => $request->first_name ,
            'last_name' => $request->last_name ,
            'code' => $request->code ,
            'point' => $request->point ,
        ]);
        return redirect()->route('students.index')->with('success' , 'عملیات ,ویرایش با موفقیت انجام شد');
    }


}

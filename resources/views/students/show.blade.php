@extends('layouts.master')

@section('title' , 'show' )

@section('content')
    <section class="show-back">

        <h1 class="ordi text-secondary text-center py-3">اطلاعات و ویرایش اطلاعات ئانش آموزان</h1>

        <a class="main ordi mb-4" href="{{route('studentHome')}}">
            <section>
          خانه

                <div class="top-border"></div>
                <div class="bottom-border"></div>

            </section>
        </a>

        <section class="d-flex justify-content-end p-4">
            <a href="{{route('students.create')}}" class="nazanin btn btn-light fw-bold fs-3">افزودن دانش آموز جدید</a>
        </section>

        @if($message = Session::get('success'))
            <section class="alert alert-success ordi">
                <p class="w-100 text-center fs-3">{{$message}}</p>
            </section>
        @endif

        <table class="table table-hover table-dark" dir="rtl">
            <tr class="text-light text-center ordi fs-2">
                <td>نام</td>
                <td>نام خانوادگی</td>
                <td>کد دانش آموزی </td>
                <td>نمره</td>
                <td>ویرایش</td>
                <td>حذف</td>
            </tr>

            @foreach($students as $student)
            <tr class="text-light text-center nazanin fs-3">
                <td>{{ $student['first_name']  }}</td>
                <td>{{ $student['last_name']  }}</td>
                <td>{{ $student['code']  }} </td>
                <td>{{ $student['point']  }}</td>
                <td><a href="{{route('students.edit' , $student->id)}}">
                        <img src="../images/signs/edit_icon.svg" width="40px" alt="dede">
                    </a></td>
                <td>
                    <form action="{{route('students.destroy' , $student->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger ordi fw-bolder fs-4" value="حذف کردن">
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    </section>
@stop

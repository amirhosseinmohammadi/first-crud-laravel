@extends('layouts.master')

@section('title' , 'show' )

@section('content')
    <section class="show-back">

        <h1 class="ordi text-secondary text-center py-3">اطلاعات و ویرایش اطلاعات ئانش آموزان</h1>

        <a class="main ordi mb-4" href="{{route('studentHome')}}">
            <section>
                خانه

                <div class="top-border"></div>
                <div class="bottom-border"></div>

            </section>
        </a>

        <section class="w-50 text-light text-center bg-success mx-auto p-4">
            <p class=" fw-bold bg-success fs-2 ordi">نفرات برتر</p>
        <table class="table table-hover table-dark mb-0" dir="rtl">
            <tr class="text-light text-center ordi fs-2">
                <td>نام</td>
                <td>نام خانوادگی</td>
                <td>نمره</td>
                <td>کد دانش آموزی </td>
            </tr>
{{--            {{dd($students)}}--}}
            @foreach($students as $student)
                <tr class="text-light text-center nazanin fs-3">
                    <td>{{ $student->first_name  }}</td>
                    <td>{{ $student->last_name  }}</td>
                    <td class="text-success fw-bolder">{{ $student->point  }} </td>
                    <td>{{ $student->code  }} </td>
                </tr>
            @endforeach
        </table>
        </section>

        <section class="w-50 bg-danger mx-auto p-4 mt-4">
            <p class=" fw-bold bg-danger text-center text-light fs-2 ordi">پایین ترین نمرات</p>
            <table class="table table-hover table-dark mb-0" dir="rtl">
                <tr class="text-light text-center ordi fs-2">
                    <td>نام</td>
                    <td>نام خانوادگی</td>
                    <td>نمره</td>
                    <td>کد دانش آموزی </td>
                </tr>
                {{--            {{dd($students)}}--}}
                @foreach($low as $lowest)
                    <tr class="text-light text-center nazanin fs-3">
                        <td>{{ $lowest->first_name  }}</td>
                        <td>{{ $lowest->last_name  }}</td>
                        <td class="text-danger fw-bolder">{{ $lowest->point  }} </td>
                        <td>{{ $lowest->code  }} </td>
                    </tr>
                @endforeach
            </table>
        </section>

        <section class="d-flex justify-content-between w-75 bg-light text-danger mx-auto my-5">
            <p class="fs-1 ordi ">{{$avg}}</p>
            <p class="fs-1 ordi text-dark">میانگین نمرات</p>
        </section>



    </section>
@stop


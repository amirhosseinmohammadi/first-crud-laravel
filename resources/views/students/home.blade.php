@extends('layouts.master')

@section('title' , 'portable' )

@section('content')
    <section class="background">
        <div class="w-100 h-100">
            <section class="w-100 h-100">
                <h1 class="ordi text-light text-center display-2 pt-5">پورتابل دانش آموزی</h1>

                <section class="d-flex justify-content-center mt-5 pt-5">
                    <a class="mt-5 mx-5 d-flex flex-column portabl-item align-items-center fs-1" href="{{route('students.index')}}">
                        <img src="../images/signs/edit.svg" class="text-center" alt="edit.sign">
                        <p class="text-center text-light nazanin">ویرایش اطلاعات دانش آموزان</p>
                    </a>

                    <a class="mt-5 mx-5 d-flex  portabl-item flex-column align-items-center fs-1" href="{{route('student.show')}}">
                        <img src="../images/signs/result.svg" width="130px" class="text-center" alt="edit.sign">
                        <p class="text-center text-light nazanin">دیدن نتایج و نمرات</p>
                    </a>

                </section>

            </section>
        </div>
    </section>
@stop

@extends('layouts.master')

@section('title' , 'Create' )

@section('content')
    <section class="w-100">
        <section class="w-50 mt-5 mx-auto">
            <form action="{{route('students.store')}}" method="post" dir="rtl">
                @csrf
                <label for="" class="ordi fs-3">نام</label>
                <input type="text" name="first_name" class="form-control nazanin fw-bolder fs-4">

                <label for="" class="ordi fs-3 mt-3"> نام خانوادگی </label>
                <input type="text" name="last_name" class="form-control nazanin fw-bolder fs-4">

                <label for="" class="ordi fs-3 mt-3"> نمره </label>
                <input type="number" name="point" class="form-control nazanin fw-bolder fs-4">

                <label for="" class="ordi fs-3 mt-3">کد </label>
                <input type="number" name="code" class="form-control nazanin fw-bolder fs-4">

                <input type="submit" class="btn btn-dark ordi px-4 py-1 mt-4 fs-4" value="ثبت">

            </form>

            <p class="ordi fw-bolder fs-2  mt-4 text-danger w-100 text-end">در صورت انجام شدن عملیات حتما پیامی مبنی بر موفقیت آمیز بودن آن دریافت خواهید کرد</p>

        </section>
    </section>
@stop

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @section('css-link')
    <link rel="stylesheet" href="{{asset('dist/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/style.css')}}">
    @show
</head>
<body>

     <!-----------sTart template----------->
     <section class="w-100">

         @yield('content')
     </section>
     <!-----------eNd template----------->




<!----start js------->
     @section('js-link')
        <script src="{{asset('dist/bootstrap/bootstrap.min.js')}}"></script>
     @show

</body>
</html>
